import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";




export default class AutoPlay extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      speed: 2000,
      autoplaySpeed: 2000,
      cssEase: "linear"
    };
    return (
      <div>
        <h2>Auto Play</h2>
        <Slider {...settings}>
          <div>
          <img src="https://i2.wp.com/www.aupp.edu.kh/wp-content/uploads/2015/03/Logo-Seal-Clean.png?ssl=1" />
          </div>
          <div>
          <img src="https://i2.wp.com/www.aupp.edu.kh/wp-content/uploads/2015/03/Logo-Seal-Clean.png?ssl=1" /> 
          </div>
          <div>
          <img src="https://i2.wp.com/www.aupp.edu.kh/wp-content/uploads/2015/03/Logo-Seal-Clean.png?ssl=1" /> 
          </div>
          <div>
          <img src="https://i2.wp.com/www.aupp.edu.kh/wp-content/uploads/2015/03/Logo-Seal-Clean.png?ssl=1" /> 
          </div>
          <div>
          <img src="https://i2.wp.com/www.aupp.edu.kh/wp-content/uploads/2015/03/Logo-Seal-Clean.png?ssl=1" /> 
          </div>
          <div>
          <img src="https://i2.wp.com/www.aupp.edu.kh/wp-content/uploads/2015/03/Logo-Seal-Clean.png?ssl=1" /> 
          </div>
        </Slider>
      </div>
    );
  }
}