import React, { Component } from "react";
import {
  Navbar,
  NavDropdown,
  Nav,
  Breadcrumb,
  Button,
  Card,
} from "react-bootstrap";
import "./Uni_detail.css";
import { GrLocation } from 'react-icons/gr';
import Slide from "./Multi_Slide/Slide";


export default class Uni_detail extends Component {
  render() {
    return (
      <div className="body">
        <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
          <Navbar.Brand href="#home">MyChoice</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="#features">Home</Nav.Link>
              <NavDropdown title="University" id="collasible-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">
                  Public School
                </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">
                  Private School
                </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">
                  Training School
                </NavDropdown.Item>
                <NavDropdown.Divider />
              </NavDropdown>
              <Nav.Link href="#deets">Major</Nav.Link>
              <Nav.Link href="#deets">Share Experience</Nav.Link>
              <Nav.Link href="#deets">Quiz Test</Nav.Link>
              <Nav.Link href="#deets">SYS</Nav.Link>
            </Nav>
            <Nav>
              <Nav.Link href="#deets">About Us</Nav.Link>
              <Nav.Link eventKey={2} href="#memes">
                Sign Out
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <div className="image">
          <div className="box">
            <div class="media">
              <img
                class="mr-3"
                width="200px"
                height="200px"
                src="https://i2.wp.com/www.aupp.edu.kh/wp-content/uploads/2015/03/Logo-Seal-Clean.png?ssl=1"
                alt="Generic placeholder image"
              />
              <div class="media-body">
                <div>
                  <h1 className="Name">American University of Phnom Penh</h1>
                  <h2 className="Name">Price Range: 6000$ - 9000$</h2>
                  <h6 className="Name">
                  <GrLocation /> Kilometer 6, 278H Street
                    201R <br />
                    Kroalkor Village, <br />
                    Sangkat, Phnom Penh
                  </h6>
                </div>
              </div>
            </div>
            <Button variant="outline-light" size="lg">
              View Website
            </Button>
          </div>
        </div>
        <Breadcrumb>
          <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
          <Breadcrumb.Item href="#">Univeristy</Breadcrumb.Item>
          <Breadcrumb.Item active>Univeristy Deatails</Breadcrumb.Item>
        </Breadcrumb>
        <div className="middle-content">
          <div>
            <p className="Title">About</p>
            <hr />
            <p className="Content">
              Originally established in 2013, the first campus of the American
              University of Phnom Penh (AUPP) was located in Tuol Kork district
              and got relocated to a new campus in 2017 at Russei Keo district.
              AUPP is a private school designed with an international university
              standard outlook, with a majority of international PhD holders as
              providers of knowledge.
            </p>
          </div>

          <div>
            <p className="Title">Programs</p>
            <hr />
            <p className="Content">
              + Bachelor of Arts in Architecture <br />
              + Bachelor of Science in Business Administration <br />
              + Bachelor of Science in Civil Engineering <br />
              + Bachelor of Arts in Global Affairs <br />
              + Bachelor of Science in Information Technology Management <br />
              + Bachelor of Arts in Law <br />+ Bachelor of Business
              Administration in Tourism and Hospitality Management
            </p>
          </div>

          <div>
            <p className="Title">Images</p>
            <hr />
            {/* <p className="Content">
              + Bachelor of Arts in Architecture <br />
              + Bachelor of Science in Business Administration <br />
              + Bachelor of Science in Civil Engineering <br />
              + Bachelor of Arts in Global Affairs <br />
              + Bachelor of Science in Information Technology Management <br />
              + Bachelor of Arts in Law <br />+ Bachelor of Business
              Administration in Tourism and Hospitality Management
            </p> */}
            <Slide/>
          </div>

          <div className="row">
            <div className="col-lg-6">
              <Card>
                <Card.Body className="text-center">
                  <Card.Title>
                    <h1 className="Title">Contact Us</h1>
                  </Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">
                    
                  </Card.Subtitle>
                  <Card.Text>
                    <h1 className="Title">+ 885 345 889</h1>
                    <h1 className="Title">+ 885 345 889</h1>
                  </Card.Text>
                </Card.Body>
              </Card>
            </div>

            <div className="col-lg-6 col-md-6 col-sm-6">
              <Card>
                <Card.Body>
                  <Card.Title className="text-center">
                    <h1 className="Title">Campus Location</h1>
                  </Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">
                    <div className="map">
                      <iframe
                        variant="top"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2913.719127580149!2d104.90234758628792!3d11.615145336421088!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31095177ee61fd7d%3A0xa66cfdf83a58b2ae!2sAmerican%20University%20of%20Phnom%20Penh!5e0!3m2!1sen!2skh!4v1595151929291!5m2!1sen!2skh"
                        frameborder="0"
                        width="520px"
                        height="300px"
                        style={{ border: 0 }}
                        allowfullscreen=""
                        aria-hidden="false"
                        tabindex="0"
                      ></iframe>
                    </div>
                  </Card.Subtitle>
                  <Card.Text>
                    <p className="Content">
                    <GrLocation />Kilometer 6, <br/>
                      278H Street 201R Kroalkor Village, <br/>
                      Sangkat, Phnom Penh
                    </p>
                  </Card.Text>
                </Card.Body>
              </Card>
            </div>
          </div>
        </div>
       
      </div>
      
      
    );
  }
}
